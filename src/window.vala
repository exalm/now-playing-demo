[GtkTemplate (ui = "/org/example/NowPlaying/window.ui")]
public class Nowplaying.Window : Hdy.ApplicationWindow {
    [GtkChild]
    private Hdy.Deck deck;
    [GtkChild]
    private Gtk.EventBox event_box;

    private Gtk.GestureMultiPress gesture;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        gesture = new Gtk.GestureMultiPress (event_box);

        gesture.released.connect (() => {
            deck.navigate (Hdy.NavigationDirection.FORWARD);
        });
    }

    [GtkCallback]
    private void close_now_playing () {
        deck.navigate (Hdy.NavigationDirection.BACK);
    }
}
